export const products = [
    {
        name: "Butter",
        price: "3.22 $",
        quantity: 1
    },
    {
        name: "Milk",
        price: "2.50 $",
        quantity: 2
    },
    {
        name: "Bread",
        price: "1.25 $",
        quantity: 1
    },
    {
        name: "Peach",
        price: "0.72 $",
        quantity: 3
    },
    {
        name: "Apple",
        price: "0.5 $",
        quantity: 4
    },
    {
        name: "Banana",
        price: "0.56 $",
        quantity: 6
    }
]