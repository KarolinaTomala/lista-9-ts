class Product {
    constructor(name) {
        this.name = name;
        this.price = this.randPrice(1, 5);
        this.quantity = this.randQuantity(0, 10);
    }

    randPrice(min, max) {
        return Math.round((Math.random() * (max - min) + min)*100)/100 + " $";
    }

    randQuantity(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    }
}

export default Product;