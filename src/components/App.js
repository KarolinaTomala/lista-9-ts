import React, {Component} from 'react';
import ProductList from './ProductList';
import AddProduct from './AddProduct';
import Product from "./Product";
import './styles/App.css';
import {products} from '../products';

class App extends Component {
    render() {
        return (
            <div className="App">
                <ProductList products = {this.state.products}/>
                <AddProduct handleOnClick={this.handleOnClick}/>
            </div>
        );
    }

    constructor(props) {
        super(props);
        this.state = {products};

        this.handleOnClick = this.handleOnClick.bind(this);
    }

    handleOnClick() {
        let name = document.getElementById("enteredName").value;
        if (name === undefined || name === "") {
            alert("Product name was not entered!");
            return;
        }
        let product = new Product(name);
        this.setState((state, props) => {
            let temporaryState = state.products;
            temporaryState.push(product);
            return {products: temporaryState};
        });
    }
}
export default App;
