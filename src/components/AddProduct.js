import React from 'react';
import './styles/AddProduct.css';

const AddProduct = ({handleOnClick}) => {
    return (
       <div className="add-product">
           <input type="text" id="enteredName" placeholder="Enter product name..."/>
           <button onClick={handleOnClick}>Add product</button>
       </div>
    );
}

export default AddProduct;