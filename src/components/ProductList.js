import React from 'react';
import './styles/ProductList.css';
import ProductCell from "./ProductCell";

const ProductList = ({products}) => {
    const cells = products.map(product => {
        return (
            <ProductCell
                product={product}
            />
        );
    });

    return (
        <section className="product-list">
            {cells}
        </section>
    )
}

export default ProductList;