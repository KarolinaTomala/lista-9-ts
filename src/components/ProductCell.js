import React from 'react';
import './styles/ProductCell.css';

const ProductCell = ({product}) => {
    const {name, price, quantity} = product;

    return (
        <section className="product-cell">
            <div className='data-wrapper'>
                <h1 className='data-name'>{name}</h1>
                <p className="data-char">Price: <span className="color-text">{price}</span></p>
                <p className="data-char">Quantity: <span className="color-text">{quantity}</span></p>
            </div>
        </section>
    )
};

export default ProductCell;

